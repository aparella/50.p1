// Alexander Parella, Kelvin Lu
/** 
 * Converter.out
 * Takes in integers in various base representations
 * Converts integers into other bases and outputs 
 * result.
 * 
 * One file as per specification.
 */

#include <iostream>

using namespace std;

/***************************************************************************
*                             UTILITY FUNCTIONS                            *
***************************************************************************/

/**
 * get_int_value()
 * Homespun FUNCTION_THAT_SHALL_NOT_BE_NAMED()
 */
unsigned short get_int_value(char value)
{
	switch(value)
	{
		// Non hex values
		case '0' : return 0;
		case '1' : return 1;
		case '2' : return 2;
		case '3' : return 3;
		case '4' : return 4;
		case '5' : return 5;
		case '6' : return 6;
		case '7' : return 7;
		case '8' : return 8;
		case '9' : return 9;
		
		// Hex values
		case 'A' : return 10;
		case 'B' : return 11;
		case 'C' : return 12;
		case 'D' : return 13;
		case 'E' : return 14;
		case 'F' : return 15;
	} // switch
	
	return 0; // Should never happen, just to suppress compiler warnings
} // get_int_value()

/** 
 * get_char_value()
 * homesupon OTHER_FUNCTION_THAT_SHALL_NOT_BE_NAMED()
 */
char get_char_value(unsigned short value)
{
	switch(value)
	{
		// Non hex values
		case 0 : return '0';
		case 1 : return '1';
		case 2 : return '2';
		case 3 : return '3';
		case 4 : return '4';
		case 5 : return '5';
		case 6 : return '6';
		case 7 : return '7';
		case 8 : return '8';
		case 9 : return '9';
		
		// Hex values
		case 10 : return 'A';
		case 11 : return 'B';
		case 12 : return 'C';
		case 13 : return 'D';
		case 14 : return 'E';
		case 15 : return 'F';
	} // get_char_value()
	
	return '\0'; // Just to suppress compiler warnings
} // get_char_value()

/**
 * reverse()
 * Reverses input_string, requires length of string as argument
 */
void reverse_array(char*& input_string, unsigned string_length)
{
	if (!string_length) return;

	char* local_copy = new char[string_length--];
	unsigned index;
	
	for(index = 0; index <= string_length; index++)
		local_copy[index] = input_string[string_length - index];
	
	local_copy[index] = '\0';
	
	for (index = 0; index <= string_length; index++)
		input_string[index] = local_copy[index];
	
	delete local_copy;
} // reverse_array()


/**
 * expand_array()
 * Expands dynamically allocated character array
 */
template <class T>
void expand_array(T*& array_ptr, unsigned int& array_size)
{
	// Create new array double in size
	T* new_array_ptr;
	new_array_ptr = new T[array_size * 2];

	// Copy old elements
	unsigned int i;
	for (i = 0; i < array_size; i++) 
		new_array_ptr[i] = array_ptr[i];

	// Release array_ptr
	delete array_ptr;

	// Update arguments
	array_ptr = new_array_ptr;
	array_size *= 2;
} // expand_array()

/**
 * get_input_unsigned()
 * Gets input from user for unsigned conversion
 * Sets values for from_base, to_base, num_chars, and size_chars, returns array for deallocation
 */
char* get_input_unsigned(unsigned short& from_base, unsigned short& to_base, unsigned& num_chars, unsigned& size_chars)
{
	char* input_string = new char[10];
	char temp_char;
	
	// Initializations
	num_chars = 0;
	size_chars = 10;
	
	// Read characters from stdin into char[]
	// We may assume no invalid characters are provided
	cout << "Number: ";
	
	cin.ignore(1); // eat newline, not bad style, sean does it SO STOP JUDGING ME
	
	// Get input in a safe manner
	temp_char = cin.get();
	while (temp_char != '\n')
	{
		input_string[num_chars] = temp_char;
		
		if (++num_chars >= size_chars) 
		{
			// What is this cout for? Debugging?
			// cout << num_chars;
			expand_array(input_string, size_chars);
		} // if()
		temp_char = cin.get();
	} // while()
	
	input_string[num_chars] = '\0'; // NULL terminate string
	
	cout << "Source base: ";
	cin >> from_base;
	
	cout << "Destination base: ";
	cin >> to_base;
	
	return input_string;
} // get_input()

/**
 * get_input_signed()
 * Gets input from user for signed conversion
 * Sets values for from_base, to_base, num_chars, and size_chars, returns array for deallocation
 */
char* get_input_signed(bool& negative, unsigned& num_chars, unsigned& size_chars)
{
	char* input_string = new char[10];
	char temp_char;
	
	// Initializations
	num_chars = 0;
	size_chars = 10;
	
	// Read characters from stdin into char[]
	// We may assume no invalid characters are provided
	cout << "Signed number: ";
	
	cin.ignore(1); // eat newline, not bad style, sean does it SO STOP JUDGING ME
	
	// Get input in a safe manner
	// Check first character for "-"
	negative = false;
	temp_char = cin.get();
	if (temp_char == '-')
	{
		negative = true;
		temp_char = cin.get();
	}

	while (temp_char != '\n')
	{
		input_string[num_chars] = temp_char;
		
		if (++num_chars >= size_chars) 
		{
			cout << num_chars;
			expand_array(input_string, size_chars);
		} // if()
		temp_char = cin.get();
	} // while()
	
	input_string[num_chars] = '\0'; // NULL terminate string
	
	return input_string;
} // get_input()

/**
 * get_float_input
 * Gets float value from user
 * Returns pointer for deallocation
 */
char* get_float_input(bool& negative, unsigned& num_chars, unsigned& size_chars)
{
	// not sure what the maximum value of the input will be
	// need to change to accomodate it
	char* input = new char[10];
	char character;

	num_chars = 0;
	size_chars = 10;

	cout << "Float: ";

	cin.ignore(1);

	negative = false;
	character = cin.get();
	if (character == '-')
	{
		negative = true;
		character = cin.get();
	}

	while(character != '\n')
	{
		input[num_chars++] = character;

		if (num_chars == size_chars)
		{
			expand_array(input, size_chars);
		} // if()

		character = cin.get();
	} // while()

	return input;
} // get_float_input

/**
 * convert_to_decimal()
 * Take in input string and convert it into its decimal value
 * Returns decimal value
 */
unsigned long long convert_to_decimal(char* input_string, unsigned short from_base, unsigned num_chars)
{	
	unsigned index;
	unsigned long long decimal = 0;
	
	for (index = 0; index < num_chars; index++)
		decimal = (decimal * from_base) + get_int_value(input_string[index]);

	return decimal;
} // convert_to_decimal()

/**
 * convert_fraction_to_decimal()
 * Take in input string and convert it into its decimal value
 * Returns decimal value
 */
double convert_fraction_to_decimal(char* input_string, unsigned short from_base, unsigned num_chars)
{	
	double decimal = 0;

	while (num_chars--)
		decimal = (decimal + get_int_value(input_string[num_chars])) / from_base;
	
	return decimal;
} // convert_to_decimal()

/**
 * negate_2s_comp()
 * Take in input string and negate it by 2's complement
 * Mutates array in place
 */
void negate_2s_comp(unsigned short base, unsigned symbols, char* char_array)
{
	unsigned i;
	unsigned short max_10_rep;
	unsigned short new_value_10_rep;
	unsigned short carryover_10_rep;


	max_10_rep = base - 1;

	// Take complement of char_array
	for (i = 0; i < symbols; i++)
	{
		char_array[i] = get_char_value(max_10_rep - get_int_value(char_array[i]));
	} // for()

	// Add one, ignore any carryover from leftmost bit
	carryover_10_rep = 1;

	for (i = symbols - 1; ; i--)
	{
		new_value_10_rep = get_int_value(char_array[i]) + carryover_10_rep;
		if (new_value_10_rep > max_10_rep)
		{
			carryover_10_rep = new_value_10_rep - max_10_rep;
			new_value_10_rep -= max_10_rep;
		} // if()
		else
		{
			carryover_10_rep = 0;
		} // else()
		char_array[i] = get_char_value(new_value_10_rep);

		if (!i) break; // i is unsigned. When i = 0 and i-- is called, i will not be -1. Thus we must do a naive break.
	}	
}

/**
 * negate_signed_mag_hex()
 * Take in hex input string and change the first symbol as to flip the first bit
 * Mutates array in place
 * We only support this functionality for hex strings for lack of requirement
 */
void negate_signed_mag_hex(char* char_array)
{
	switch(char_array[0])
	{
		case '0' : char_array[0] = '8'; break;
		case '1' : char_array[0] = '9'; break;
		case '2' : char_array[0] = 'A'; break;
		case '3' : char_array[0] = 'B'; break;
		case '4' : char_array[0] = 'C'; break;
		case '5' : char_array[0] = 'D'; break;
		case '6' : char_array[0] = 'E'; break;
		case '7' : char_array[0] = 'F'; break;
		case '8' : char_array[0] = '0'; break;
		case '9' : char_array[0] = '1'; break;
		case 'A' : char_array[0] = '2'; break;
		case 'B' : char_array[0] = '3'; break;
		case 'C' : char_array[0] = '4'; break;
		case 'D' : char_array[0] = '5'; break;
		case 'E' : char_array[0] = '6'; break;
		case 'F' : char_array[0] = '7'; break;
	}
}

/***************************************************************************
*                            CONVERSION FUNCTIONS                          *
***************************************************************************/

void convertUnsigned()
{
	char* input_string;             // Holds input number as a string of 
	char* output_string;              // characters assumed to be from 0-F.
    
	// We will assume there is a maximum of ~2b characters
	unsigned int num_chars;          // Number of characters currently in array
	unsigned int size_chars;         // Number of characters array can hold
	
	unsigned short from_base, to_base;	// Assumed bases are in [2, 16]
	
	unsigned index;
	unsigned long long input_in_decimal;
	
	input_string = get_input_unsigned(from_base, to_base, num_chars, size_chars);
	
	// Calculate value of input in binary as middle step
	input_in_decimal = convert_to_decimal(input_string, from_base, num_chars);
	if (input_in_decimal == 0)
	{
		cout << "0" << endl << endl;
		return;
	} // if()
	
	output_string = new char[num_chars + 1];
	
	// Horner's algorithm
	// NOTE: We now use num_chars to represent the elements of output_string, not input_string
	index = 0;
	while(input_in_decimal)
	{
		output_string[index++] = get_char_value(input_in_decimal % to_base);
		input_in_decimal /= to_base;
		
		if (index == num_chars) // If array needs to be expanded, expand it
			expand_array(output_string, num_chars); 
	} // while()
	
	// NULL terminate string
	output_string[index] = '\0';
	
	// Reverse and output result
	// Must reverse because Horner's algorithm outputs LSD first
	reverse_array(output_string, index);
	cout << output_string << endl << endl;
	
	delete input_string; // Clean up
	delete output_string;
} // convertUnsigned()

void convertSigned()
{
	char* input_string;            								 	// Holds input number as a string of 
	char output_string_signed_mag[9], output_string_2s_comp[9];   // characters assumed to be from 0-F.

	// We will assume there is a maximum of ~2b characters
	unsigned int num_chars;          // Number of characters currently in array
	unsigned int size_chars;         // Number of characters array can hold
	
	// unsigned index;
	unsigned long long input_in_decimal;
	
	char temp_char;
	unsigned short index;
	bool negative;


	// Get decimal integer as string, then convert to C++ long long
	input_string = get_input_signed(negative, num_chars, size_chars);
	input_in_decimal = convert_to_decimal(input_string, 10, num_chars);

	// Make sure the char arrays are null terminated
	output_string_signed_mag[8] = '\0';
	output_string_2s_comp[8] = '\0';

	// Horner's algorithm
	// NOTE: We write right to left, preserving big endian order
	index = 8;
	while(input_in_decimal && index--) // Prevent buffer overflow/seg. fault
	{
		temp_char = get_char_value(input_in_decimal % 16);
		output_string_signed_mag[index] = temp_char;
		output_string_2s_comp[index] = temp_char;
		input_in_decimal /= 16;
	} // while()

	// Set remaining bits on left to 0
	while (index--)
	{
		output_string_signed_mag[index] = '0';
		output_string_2s_comp[index] = '0';		
	} // while()

	if (negative)
	{
		negate_signed_mag_hex(output_string_signed_mag);
		negate_2s_comp(16, 8, output_string_2s_comp);
	} // if()
 
	cout << "Signed magnitude: " << output_string_signed_mag << endl;
	cout << "Two's complement: " << output_string_2s_comp << endl << endl;

	delete input_string;
} // convertSigned()

void convertFloat2()
{
	// IEEE representation requires 32 bits, use character array instead
	// Except we don't return the 32 bit bitset ahahahahahahahhaha

	// Declarations
	char* input_string;
	unsigned num_chars, size_chars;
	bool negative;

	// Get characters without sign and determine sign
	input_string = get_float_input(negative, num_chars, size_chars); 
	
	// Split input_string into a natural number string and a fraction string
	unsigned i;

	char* natural_string;
	unsigned natural_num_chars;
	char* fraction_string;
	unsigned fraction_num_chars;

	for (i = 0; i < num_chars; i++)
		if (input_string[i] == '.')
			break;

	// If no period was found, i = num_chars. Thus natural_num_chars = num_chars.
	natural_string = input_string;
	natural_num_chars = i;
	

	// If no period was found, i = num_chars. Thus fraction_num_chars = -1 or 4294967294.
	fraction_string = input_string + i + 1;
	if (i == num_chars) 
		fraction_num_chars = 0; 
	else 
		fraction_num_chars = num_chars - i - 1;

	// Find decimal representations of the natural number string and fraction string
	unsigned long long natural_decimal;
	double fraction_decimal;
 	natural_decimal = convert_to_decimal(natural_string, 10, natural_num_chars);
 	fraction_decimal = convert_fraction_to_decimal(fraction_string, 10, fraction_num_chars);

 	// Get the decimal representation
 	float float_decimal;
 	float_decimal = (negative ? -1 : 1) * (natural_decimal + fraction_decimal);

 	// u_int32 spoofs
 	unsigned float_spoof, mantissa_spoof, exponent_spoof;
 	int exponent_int_spoof;
 	bool exponent_negative;

 	float_spoof = *((unsigned *) &float_decimal);
	mantissa_spoof = float_spoof & 8388607; // 8388607 = 0b11111111111111111111111, 23 bit mask
	exponent_spoof = (float_spoof >> 23) & 255; // 255 = 0b11111111, 8 bit mask

	// Normalize exponent
	if (exponent_spoof > 127) 
	{
		exponent_spoof = exponent_spoof - 127;
		exponent_negative = false;
	} 
	else if (exponent_spoof > 0)
	{
		// We perform the offset subtraction and negation on an integer, since
		// doing the same things on an unsigned integer gives erroneous results
		exponent_int_spoof = exponent_spoof; 
		exponent_int_spoof -= 127;
		exponent_int_spoof *= -1;
		exponent_spoof = exponent_int_spoof;
		exponent_negative = true;
	}
	else
	{
		// If the exponent is 0, then we have the special case.
		cout << "0.0 E0" << "\n" << endl;
		return;
	}

	 // Horner's algorithm for mantissa
 	unsigned mantissa_num_chars;
 	char* mantissa_string; 
 	mantissa_string = new char[24]; // 23 + 1, for null terminating character
	mantissa_num_chars = 0;
	while (mantissa_spoof)
	{
		mantissa_string[mantissa_num_chars++] = get_char_value(mantissa_spoof % 2);
		mantissa_spoof /= 2;
		// We shall assume mantissa_spoof can fit into 23 bits, since it has been masked
	} // while()
	while (mantissa_num_chars < 23) 
		mantissa_string[mantissa_num_chars++] = '0';
	reverse_array(mantissa_string, mantissa_num_chars);
	mantissa_string[23] = '\0';

	 // Horner's algorithm for exponent
	unsigned exponent_num_chars;
 	char* exponent_string; 
 	exponent_string = new char[9]; // 8 + 1, for null terminating character
 	exponent_num_chars = 0;
	while (exponent_spoof)
	{
		exponent_string[exponent_num_chars++] = get_char_value(exponent_spoof % 2);
		exponent_spoof /= 2;
		// We shall assume exponent_spoof can fit into 8 bits, since it has been masked
	} // while()

	exponent_string[exponent_num_chars] = '\0';
	reverse_array(exponent_string, exponent_num_chars);

	// Display results
	cout << (negative ? "-" : "") << "1." << mantissa_string << " " << "E" << (exponent_negative ? "-" : "") << exponent_string << "\n" << endl;

	// All deallocations
	delete mantissa_string;
	delete exponent_string;
	delete input_string;
} // convertFloat2()

void convertFloat()
{
	// IEEE representation requires 32 bits, use character array instead
	// Except we don't return the 32 bit bitset ahahahahahahahhaha

	// Declarations
	char* input_string;
	unsigned num_chars, size_chars;
	bool negative;

	// Get characters without sign and determine sign
	input_string = get_float_input(negative, num_chars, size_chars); 
	
	// Split input_string into a natural number string and a fraction string
	unsigned i, j, k;

	char* natural_string;
	unsigned natural_num_chars, natural_size_chars;
	char* fraction_string;
	unsigned fraction_num_chars;

	for (i = 0; i < num_chars; i++)
		if (input_string[i] == '.')
			break;

	// If no period was found, i = num_chars. Thus natural_num_chars = num_chars.
	natural_string = input_string;
	natural_num_chars = i;
	

	// If no period was found, i = num_chars. Thus fraction_num_chars = -1 or 4294967294.
	fraction_string = input_string + i + 1;
	if (i == num_chars) 
		fraction_num_chars = 0; 
	else 
		fraction_num_chars = num_chars - i - 1;

	// Find decimal representations of the natural number string and fraction string
	unsigned long long natural_decimal;
	double fraction_decimal;
 	natural_decimal = convert_to_decimal(natural_string, 10, natural_num_chars);
 	fraction_decimal = convert_fraction_to_decimal(fraction_string, 10, fraction_num_chars);

 	// Testing
 	// cout << "Natural: " << natural_decimal << endl;
 	// cout << "Fraction: " << fraction_decimal << endl;

 	// Get binary representations of the natural number string
 	// For efficency, we reuse natural_string and natural_num_chars

 	// Horner's algorithm for integral numbers
	natural_num_chars = 0;
 	natural_size_chars = 10;
 	natural_string = new char[natural_size_chars];
	while(natural_decimal)
	{
		natural_string[natural_num_chars++] = get_char_value(natural_decimal % 2);
		natural_decimal /= 2;
		
		if (natural_num_chars == natural_size_chars) // If array needs to be expanded, expand it
			expand_array(natural_string, natural_size_chars); 
	} // while()

	// Add null terminating character
	natural_string[natural_num_chars] = '\0';	

	// Reverse character array for integral Horner's algorithm
	reverse_array(natural_string, natural_num_chars);

 	// Get binary representations of the fractional number string
 	// For efficency, we reuse natural_string and natural_num_chars

 	// Horner's algorithm for fractional numbers
	fraction_num_chars = 0;
 	fraction_string = new char[152];
	while(fraction_decimal && fraction_num_chars < 152) 
	{
		fraction_decimal *= 2;
		fraction_string[fraction_num_chars++] = get_char_value((unsigned) fraction_decimal);
		if (fraction_decimal >= 1) fraction_decimal -= 1;
	} // while()

	// Fill in any remaining 0's
	while(fraction_num_chars < 152)
		fraction_string[fraction_num_chars++] = '0';

	// Add null terminating character
	fraction_string[152] = '\0';

 	// Testing
 	// cout << "Natural (binary): " << natural_string << endl;
 	// cout << "Fraction (binary): " << fraction_string << endl;

 	// Find exponent
 	int exponent;
 	exponent = 0;

 	if (natural_num_chars) 
 		exponent = natural_num_chars - 1;
 	else 
 	{
 		exponent = -1;
 		i = 0;
 		while (fraction_string[i] == '0')
 		{
 			exponent -= 1;
 			if (++i == 151) 
 				{
 					cout << "0.0 E0" << endl << endl;
 					return;
 				}
 		}
 	}

 	// Form final output
 	char mantissa_string[24];

 	// Concatenate the natural string and the fractional string
 	j = 0;
 	if (natural_num_chars)
 		for (k = 1; k < natural_num_chars && j < 23; k++)
 			mantissa_string[j++] = natural_string[k];

 	if (natural_num_chars)
 		k = 0;
 	else
 		k = i + 1;

 	for (; k < 152 && j < 23; k++)
 		mantissa_string[j++] = fraction_string[k];

 	mantissa_string[23] = '\0';

 	// Convert decimal exponent to binary string
 	bool exponent_negative;
 	unsigned exponent_num_chars;
 	char* exponent_string;

 	exponent_negative = false;
 	if (exponent < 0) 
	{
 		exponent_negative = true;
 		exponent *= -1;
	}

 	exponent_num_chars = 0;
 	exponent_string = new char[9];
	while(exponent && exponent_num_chars < 8)
	{
		exponent_string[exponent_num_chars++] = get_char_value(exponent % 2);
		exponent /= 2;
	} // while()	

	reverse_array(exponent_string, exponent_num_chars);


 	// Display results
 	if (negative) cout << "-";
 	cout << "1." << mantissa_string << " E";
 	if (exponent_negative) cout << "-";
 	cout << exponent_string << endl << endl;

	// IEEE_float was dynamically allocated in get_float_input, deallocate it
	// Avoid dem memory leaks
	delete input_string;
} // convertFloat()

/***************************************************************************
*                              USER INTERFACE                              *
***************************************************************************/

/**
 * getSelection
 * Presents user menu of options and returns the 
 * option selected to the calling function.
 *
 * No error handling as per specification
 */
int getSelection()
{
	int selection;
	cout << "Menu\n";
	cout << "0. Done.\n";
	cout << "1. Convert unsigned integer.\n";
	cout << "2. Convert signed integer.\n";
	cout << "3. Convert float.\n";
	cout << "Your choice: ";
	cin >> selection;
	
	return selection;
} // getSelection()

/** 
 * Main
 * While loop for continuous iteration until user selects to not continue.
 */
int main()
{
	int selection;
	
	do 
	{
		selection = getSelection();
		switch (selection) 
		{
			case 0 : cout << "Bye\n"; return 0;
			case 1 : convertUnsigned(); break;
			case 2 : convertSigned(); break;
			case 3 : convertFloat2(); break;
		};
	} while(1);
	
	return 0;
} // main()